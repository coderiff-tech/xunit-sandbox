﻿namespace Sandbox.Api;

public interface ICustomService
{
    Task<string> GetCustomResult();
}