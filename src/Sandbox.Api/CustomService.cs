﻿namespace Sandbox.Api;

public class CustomService
    : ICustomService
{
    public Task<string> GetCustomResult()
    {
        return Task.FromResult("Bar");
    }
}