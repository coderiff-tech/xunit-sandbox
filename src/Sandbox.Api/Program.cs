using Sandbox.Api;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

app.MapGet("/sample/config", (IConfiguration config) =>
{
    var value = config.GetValue<string>("Foo");
    return value;
});

app.MapGet("/sample/service", async (ICustomService service) =>
{
    var value = await service.GetCustomResult();
    return value;
});

app.Run();

public abstract partial class Program;