﻿using Sandbox.Api;

namespace Sandbox.IntegrationTests;

public class MockCustomService
    : ICustomService
{
    public Task<string> GetCustomResult()
    {
        return Task.FromResult("Biz");
    }
}