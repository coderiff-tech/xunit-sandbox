﻿// ReSharper disable ConvertToPrimaryConstructor
namespace Sandbox.IntegrationTests;

public class HttpTests
    : IClassFixture<IntegrationTestWebAppFactory>
{
    private readonly HttpClient _httpClient;

    public HttpTests(IntegrationTestWebAppFactory factory)
        => _httpClient = factory.CreateClient();

    [Fact]
    public async Task It_Should_Override_Settings()
    {
        var result = await _httpClient.GetStringAsync("sample/config");
        Assert.Equal("Biz", result);
    }
    
    [Fact]
    public async Task It_Should_Replace_Service()
    {
        var result = await _httpClient.GetStringAsync("sample/service");
        Assert.Equal("Biz", result);
    }
}