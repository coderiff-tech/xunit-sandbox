﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Sandbox.Api;
using Sandbox.IntegrationTests.Extensions;

namespace Sandbox.IntegrationTests;

public class IntegrationTestWebAppFactory
    : WebApplicationFactory<Program>
{
    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        base.ConfigureWebHost(builder);

        var testConfigurationBuilder =
            new ConfigurationBuilder()
                .AddInMemoryCollection(
                    new List<KeyValuePair<string, string?>>
                    {
                        new("Foo", "Biz")
                    })
                .Build();

        builder
            // Use custom environment Test instead of Development (optional)
            .UseEnvironment("Test")
            // Override settings for tests
            .UseConfiguration(testConfigurationBuilder)
            // Replace service with mock
            .ConfigureServices(services =>
            {
                services.RemoveRegisteredService<ICustomService>();
                services.AddSingleton<ICustomService, MockCustomService>();
            });
    }
}