﻿namespace Sandbox.UnitTests.Advanced;

public static class Collections
{
    [CollectionDefinition("A")]
    public class CollectionA : ICollectionFixture<CustomClassFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }
    
    [CollectionDefinition("B")]
    public class CollectionB : ICollectionFixture<CustomClassFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }
}
