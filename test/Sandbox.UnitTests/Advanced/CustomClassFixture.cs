﻿namespace Sandbox.UnitTests.Advanced;

public class CustomClassFixture
    : IDisposable
{
    private int _count = 0;

    public void IncreaseCounter()
    {
        _count++;
    }
    
    public int GetCounter()
    {
        return _count;
    }

    public void Dispose() => _count = 0;
}