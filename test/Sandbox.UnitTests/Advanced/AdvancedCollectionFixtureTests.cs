﻿using Xunit.Abstractions;
// ReSharper disable ConvertToPrimaryConstructor

namespace Sandbox.UnitTests.Advanced;

public static class AdvancedCollectionFixtureTests
{
    [Collection("A")]
    public class CollectionATests
    {
        private readonly CustomClassFixture _customClassFixture;
        private readonly ITestOutputHelper _output;

        public CollectionATests(
            CustomClassFixture customClassFixture,
            ITestOutputHelper output)
        {
            _customClassFixture = customClassFixture;
            _output = output;
        }
    
        [Theory]
        [InlineData("one")]
        [InlineData("two")]
        [InlineData("three")]
        public void It_Should_Run_With_Access_To_Shared_Resource(string executionName)
        {
            _customClassFixture.IncreaseCounter();
            var current = _customClassFixture.GetCounter();
            _output.WriteLine($"Current counter for test {executionName} is {current}");
            Assert.InRange(current,1, 3);
        }
    }
    
    [Collection("B")]
    public class CollectionBTests
    {
        private readonly CustomClassFixture _customClassFixture;
        private readonly ITestOutputHelper _output;

        public CollectionBTests(
            CustomClassFixture customClassFixture,
            ITestOutputHelper output)
        {
            _customClassFixture = customClassFixture;
            _output = output;
        }
    
        [Theory]
        [InlineData("one")]
        [InlineData("two")]
        [InlineData("three")]
        public void It_Should_Run_With_Access_To_Shared_Resource(string executionName)
        {
            _customClassFixture.IncreaseCounter();
            var current = _customClassFixture.GetCounter();
            _output.WriteLine($"Current counter for test {executionName} is {current}");
            Assert.InRange(current,1, 3);
        }
    }
}

