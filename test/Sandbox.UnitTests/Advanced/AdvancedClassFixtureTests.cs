﻿using Xunit.Abstractions;

namespace Sandbox.UnitTests.Advanced;

public class AdvancedClassFixtureTests
    : IClassFixture<CustomClassFixture>
{
    private readonly CustomClassFixture _customClassFixture;
    private readonly ITestOutputHelper _output;

    public AdvancedClassFixtureTests(
        CustomClassFixture customClassFixture,
        ITestOutputHelper output)
    {
        _customClassFixture = customClassFixture;
        _output = output;
    }
    
    [Theory]
    [InlineData("one")]
    [InlineData("two")]
    [InlineData("three")]
    public void It_Should_Run_With_Access_To_Shared_Resource(string executionName)
    {
        _customClassFixture.IncreaseCounter();
        var current = _customClassFixture.GetCounter();
        _output.WriteLine($"Current counter for test {executionName} is {current}");
        Assert.InRange(current,1, 3);
    }
}