using Xunit.Abstractions;

namespace Sandbox.UnitTests.Basic;

public abstract class AsyncBasicLifecycleFixture
    : IAsyncLifetime
{
    private readonly ITestOutputHelper _output;

    protected AsyncBasicLifecycleFixture(ITestOutputHelper output) 
        =>  _output = output;
    
    public Task InitializeAsync()
    {
        _output.WriteLine("InitializeAsync");
        return Task.CompletedTask;
    }

    public Task DisposeAsync()
    {
        _output.WriteLine("DisposeAsync");
        return Task.CompletedTask;
    }
}