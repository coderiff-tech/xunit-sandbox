﻿using Xunit.Abstractions;

namespace Sandbox.UnitTests.Basic;

public class AsyncBasicLifecycleTests
    : AsyncBasicLifecycleFixture, IDisposable
{
    private readonly ITestOutputHelper _output;

    public AsyncBasicLifecycleTests(ITestOutputHelper output) 
        : base(output)
    {
        _output = output;
        _output.WriteLine("Constructor");
    }

    [Fact, TestPriority(1)]
    [CustomBeforeAndAfter]
    public async Task It_Should_Run_First()
    {
        _output.WriteLine("Fact");
        await Task.Delay(1000);
        Assert.True(true);
    }
    
    [Fact, TestPriority(2)]
    [CustomBeforeAndAfter]
    public async Task It_Should_Run_Second()
    {
        _output.WriteLine("Fact");
        await Task.Delay(1000);
        Assert.False(false);
    }

    public void Dispose()
    {
        _output.WriteLine("Dispose");
    }
}