﻿namespace Sandbox.UnitTests.Basic;

[AttributeUsage(AttributeTargets.Method)]
public class TestPriorityAttribute : Attribute
{
    public int Priority { get; private set; }

    // ReSharper disable once ConvertToPrimaryConstructor
    public TestPriorityAttribute(int priority) => Priority = priority;
}