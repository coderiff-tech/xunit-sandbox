﻿using System.Reflection;
using Xunit.Sdk;

namespace Sandbox.UnitTests.Basic;

public class CustomBeforeAndAfterAttribute
    : BeforeAfterTestAttribute
{
    public override void Before(MethodInfo methodUnderTest)
    {
        base.Before(methodUnderTest);
        Console.WriteLine("this is before");
    }

    public override void After(MethodInfo methodUnderTest)
    {
        base.After(methodUnderTest);
        Console.WriteLine("this is after");
    }
}