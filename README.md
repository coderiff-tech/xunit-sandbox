# xunit-sandbox

xUnit has several features to allow the creation of a powerful testing approach.

## IAsyncLifetime
When dealing with awaitable tasks in test, implement interface `IAsyncLifetime`, which has methods that xUnit will run
and await for each test, so that the constructor and the dispose methods don't have to deal with awaitable task executions.

The lifecycle, for a test class implementing `IAsyncLifetime`, follows this sequential order:
```
Constructor
InitializeAsync
Fact
DisposeAsync
Dispose
```

## ITestOutputHelper
xUnit can automatically inject an instance of `ITestOutputHelper` in order to output text in tests.

## BeforeAfterTestAttribute
Custom attributes can be created to decorate a test so that specific actions can be executed before and/or after running
a test. It's not a good idea to inject dependencies in these custom attributes, 
see https://blogs.cuttingedge.it/steven/posts/2014/dependency-injection-in-attributes-dont-do-it/

## xUnit Runner JSON
Add a `xunit.runner.json` at the root of the xUnit project and make sure it is copied to the output folder as *Content*
This file can have a schema which will allow the IDE to display intellisense.
```
{
  "$schema": "https://xunit.net/schema/current/xunit.runner.schema.json"
}
```

Add things to, for example, customize the name of the tests and remove the underscores so that it's more readable
```
"methodDisplayOptions": "replaceUnderscoreWithSpace"
```

## ITestCaseOrderer
Implement interface `ITestCaseOrderer` to customize test order.

## Class Fixture
Used when sharing a single test context between all the tests in a class, and have it cleaned up after all the tests in
the class finish.

Create a class Fixture (e.g: `CustomClassFixture`) which could implement `IDisposable`. 

Then in the tests class implement `IClassFixture<CustomClassFixture>` where an instance of the class fixture `CustomClassFixture`
could optionally be injected and shared amongst all the Fact or Theory.

## Collection Fixture
Used when sharing a single test context between all the tests in many classes, and have it cleaned up after all the tests in
all these classes finish.

Create a class Fixture (e.g: `CustomClassFixture`) which could implement `IDisposable`. 

Create an empty class implementing `ICollectionFixture<CustomClassFixture>` and with an attribute to name its definition [CollectionDefinition("A")].

Then in one or many of the tests classes, use the attribute [Collection("A")] where an instance of the class fixture `CustomClassFixture`
could optionally be injected and shared amongst all the Fact or Theory in all the classes.

## Integration Tests
Integration tests use the nuget package *Microsoft.AspNetCore.Mvc.Testing* where the base class `WebApplicationFactory<Program>`
is extended in a fixture or factory capable of accessing DI container and creating an http client for tests.

It can override settings, services, etc.

NOTE: *Program.cs* must add a line `public abstract partial class Program;` in minimal web api so that a different assembly can 
reference the entry point class.